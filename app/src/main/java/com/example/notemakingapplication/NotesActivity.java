package com.example.notemakingapplication;

import com.example.notemakingapplication.DatabaseEntities.DatabaseNotes;
import com.example.notemakingapplication.DatabaseEntities.DatabaseSubject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class NotesActivity extends Activity 
{
	protected  DatabaseHelper mDatabase = null; 
	protected Cursor mCursor = null;
	protected SQLiteDatabase mDB = null;
	private SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	private String asColumnsToReturn[] = {
			DatabaseNotes.NOTES_ID,
			DatabaseNotes.NOTES_TITLE,
			DatabaseNotes.NOTES_DATE};
	private ListAdapter adapter = null, adapter1=null;
	private ListView av = null;
	ListView listView;
	String subject_name;
	EditText search;
	Button go;
	Intent i;
	int flag=0;
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        mDatabase = new DatabaseHelper(this.getApplicationContext());
		mDB = mDatabase.getWritableDatabase();
		i=getIntent();
		displayNotes();
		listView=(ListView)findViewById(R.id.notesList);
		go=(Button)findViewById(R.id.gobutton);
		search=(EditText)findViewById(R.id.searchText);
		go.setOnClickListener(new View.OnClickListener()
	    {
			 public void onClick(View v)
	         {
				 
				 String str=String.valueOf(search.getText());
				 
				 displaySearchResult(str);
	         }

			
	    });
		
    }
	
	
	@Override
    public void onBackPressed()
	{
		super.onBackPressed();
		if(flag==1)
       displayNotes(); 
		else
		{
			Intent j=new Intent(getApplicationContext(),SubjectActivity.class);
			j.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(j);
		}
 
        
    }
	
	private void displaySearchResult(String str) 
	{   flag=1;
		System.out.println("in display search");
		System.out.println(str);
		queryBuilder.setTables(DatabaseNotes.NOTES_TABLE_NAME);
    	mCursor = queryBuilder.query(mDB, asColumnsToReturn, DatabaseNotes.NOTES_TITLE+"=?", new String[] {str}, null,
				null,DatabaseNotes.DEFAULT_SORT_ORDER);
    	adapter1 = new SimpleCursorAdapter(this, R.layout.activity_show_subjects,
				mCursor, new String[] {  DatabaseNotes.NOTES_TITLE, DatabaseNotes.NOTES_DATE},
				new int[] { R.id.TextView_subjectname, R.id.date_time}, 1);		
		av = (ListView) findViewById(R.id.notesList);
		av.setAdapter(adapter1);
		((BaseAdapter) adapter).notifyDataSetChanged();
		System.out.println("adapter set");
		av.setOnItemClickListener(new OnItemClickListener() 
		{
            public void onItemClick(AdapterView <? > parent, View view,int position, long id) 
            {
            	TextView textView= (TextView) view.findViewById(R.id.TextView_subjectname);
            	String notes_title = textView.getText().toString();
            	Intent i=new Intent(getApplicationContext(),EditNotesActivity.class);
            	Cursor mCursor1 = queryBuilder.query(mDB, new String[] {DatabaseNotes.NOTES_ID}, DatabaseNotes.NOTES_TITLE+"=?", new String[] {notes_title}, null,
        				null,DatabaseNotes.DEFAULT_SORT_ORDER);
            	String notes_id="";
            	mCursor1.moveToFirst();
            	notes_id=mCursor1.getString(mCursor1.getColumnIndex(DatabaseNotes.NOTES_ID));
            	System.out.println("NOTES_ID"+notes_id);
            	i.putExtra("notesid", notes_id);
            	i.putExtra("subject_name", subject_name);
            	startActivity(i);	
            }
        });	
		
	}
	private void displayNotes()
	{
		flag=0;
		subject_name=i.getStringExtra("subject_name");
		queryBuilder.setTables(DatabaseNotes.NOTES_TABLE_NAME);
    	mCursor = queryBuilder.query(mDB, asColumnsToReturn, DatabaseSubject.SUBJECT_NAME+"=?", new String[] {subject_name}, null,
				null,DatabaseNotes.DEFAULT_SORT_ORDER);
    	adapter = new SimpleCursorAdapter(this, R.layout.activity_show_subjects,
				mCursor, new String[] {  DatabaseNotes.NOTES_TITLE, DatabaseNotes.NOTES_DATE},
				new int[] { R.id.TextView_subjectname, R.id.date_time}, 1);		
		av = (ListView) findViewById(R.id.notesList);
		av.setAdapter(adapter);
		av.setOnItemClickListener(new OnItemClickListener() 
		{
            public void onItemClick(AdapterView <? > parent, View view,int position, long id) 
            {
            	TextView textView= (TextView) view.findViewById(R.id.TextView_subjectname);
            	String notes_title = textView.getText().toString();
            	Intent i=new Intent(getApplicationContext(),EditNotesActivity.class);
            	Cursor mCursor1 = queryBuilder.query(mDB, new String[] {DatabaseNotes.NOTES_ID}, DatabaseNotes.NOTES_TITLE+"=?", new String[] {notes_title}, null,
        				null,DatabaseNotes.DEFAULT_SORT_ORDER);
            	String notes_id="";
            	mCursor1.moveToFirst();
            	notes_id=mCursor1.getString(mCursor1.getColumnIndex(DatabaseNotes.NOTES_ID));
            	System.out.println("NOTES_ID"+notes_id);
            	
            	i.putExtra("notesid", notes_id);
            	i.putExtra("subject_name", subject_name);
            	startActivity(i);	
            }
        });	
		av.setOnItemLongClickListener(new OnItemLongClickListener() 
		{
			
	        @Override
	        public boolean onItemLongClick(AdapterView<?> parent, final View view, int position, long id) 
	        {
	        	AlertDialog.Builder alertDialog = new AlertDialog.Builder(NotesActivity.this);
	            // Setting Dialog Title
	            alertDialog.setTitle("Delete Note");
	            // Setting Dialog Message
	            alertDialog.setMessage("Do you really want to delete this note?");
	            alertDialog.setPositiveButton("Delete",
	                    new DialogInterface.OnClickListener() 
	            {
	                        public void onClick(DialogInterface dialog,int which) 
	                        {
	                        	TextView textView= (TextView) view.findViewById(R.id.TextView_subjectname);
	                        	String notes_title = textView.getText().toString();
	                        	System.out.println("Deleted"+notes_title);
	                        	mDB.delete(DatabaseNotes.NOTES_TABLE_NAME,"notes_title=?",new String[] { notes_title });
	                        	displayNotes();
	                            
	                        }
	             });
	            // Setting Negative "NO" Button
	            alertDialog.setNegativeButton("CANCEL",
	                    new DialogInterface.OnClickListener() 
	            {
	                        public void onClick(DialogInterface dialog, int which) 
	                        {
	                            dialog.cancel();
	                        }
	                    });

	            // closed

	            // Showing Alert Message
	            alertDialog.show(); 
	            return true;
	            
	        }
	        
	    });

	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
	    // Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.notes, menu);
	     
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    int id = item.getItemId();
	    if (id == R.id.add_notes) 
	    {
	    	Intent i=new Intent(getApplicationContext(),AddNotesActivity.class);
	    	i.putExtra("subject_name", subject_name);
        	startActivity(i);
	    }
	    else if (id == R.id.sort_title) 
	    {
	    	DatabaseNotes.DEFAULT_SORT_ORDER="notes_title ASC";
	    	displayNotes();
	    }
	    else if (id == R.id.sort_date) 
	    {
	    	DatabaseNotes.DEFAULT_SORT_ORDER="notes_date ASC";
	    	displayNotes();
	    }
	    
	    return super.onOptionsItemSelected(item);
	}

}
