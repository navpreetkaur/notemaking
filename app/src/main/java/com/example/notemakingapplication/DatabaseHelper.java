package com.example.notemakingapplication;

import com.example.notemakingapplication.DatabaseEntities.DatabaseNotes;
import com.example.notemakingapplication.DatabaseEntities.DatabaseNotesFiles;
import com.example.notemakingapplication.DatabaseEntities.DatabaseNotesPictures;
import com.example.notemakingapplication.DatabaseEntities.DatabaseSubject;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper 
{
	private static final String DATABASE_NAME = "notes.db";
    private static final int DATABASE_VERSION = 1;
    SQLiteDatabase mDatabase;
	public DatabaseHelper(Context context) 
	{
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
	

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		db.execSQL("CREATE TABLE " + DatabaseSubject.SUBJECT_TABLE_NAME+ " ("
                + DatabaseSubject.SUBJECT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + DatabaseSubject.SUBJECT_NAME + " TEXT "
                + ");");
		db.execSQL("CREATE TABLE " + DatabaseNotes.NOTES_TABLE_NAME+ " ("
                + DatabaseNotes.NOTES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + DatabaseNotes.NOTES_TITLE + " TEXT ,"
                +DatabaseNotes.NOTES_DATA+" TEXT ,"
                +DatabaseNotes.NOTES_DATE+" DEFAULT CURRENT_DATE,"
                +DatabaseNotes.NOTES_TIME+" DEFAULT CURRENT_TIME, "
                +DatabaseNotes.NOTES_LATITUDE+" DOUBLE, "
                +DatabaseNotes.NOTES_LONGITUDE+" DOUBLE, "
                +DatabaseSubject.SUBJECT_NAME+" TEXT "
                + ");");
		db.execSQL("CREATE TABLE " + DatabaseNotesFiles.NOTES_FILES_TABLE_NAME+ " ("
                + DatabaseNotes.NOTES_ID + " INTEGER ,"
                +DatabaseNotesFiles.NOTES_AUDIO_PATH+" TEXT "
                + ");");
		db.execSQL("CREATE TABLE " + DatabaseNotesPictures.NOTES_PICTURES_TABLE_NAME+ " ("
                + DatabaseNotes.NOTES_ID + " INTEGER ,"
                +DatabaseNotesPictures.NOTES_PICTURE_PATH+" BLOB "
                + ");");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	

}
