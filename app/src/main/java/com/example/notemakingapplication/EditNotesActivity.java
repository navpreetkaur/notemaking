package com.example.notemakingapplication;



import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import com.example.notemakingapplication.DatabaseEntities.DatabaseNotes;
import com.example.notemakingapplication.DatabaseEntities.DatabaseNotesFiles;
import com.example.notemakingapplication.DatabaseEntities.DatabaseNotesPictures;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


public class EditNotesActivity extends Activity 
{
	private static final String DEBUG_TAG = "SimpleDB Log";
	protected  DatabaseHelper mDatabase = null; 
	protected Cursor mCursor = null;
	protected SQLiteDatabase mDB = null;
	EditText title,data;
	Double latitude,longitude;
	Intent i;
	String subject_name;
	boolean mStartPlaying = true;
	boolean mStartRecording = true;
	private MediaRecorder mRecorder = null;
    private MediaPlayer   mPlayer = null;
	Button play,record;
	String mFileName;
	byte[] mImagePath;
	ImageView image;
	int flag=0;
	String path="";
	int howToInsert=0;
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_notes); 
        title=(EditText)findViewById(R.id.notestitle);
        data=(EditText)findViewById(R.id.notesdata);
        mDatabase = new DatabaseHelper(this.getApplicationContext());
		mDB = mDatabase.getWritableDatabase();
        i=getIntent();
        subject_name=i.getStringExtra("subject_name");
        image=(ImageView)findViewById(R.id.mImageView);
        play=(Button)findViewById(R.id.play);
        play.setBackgroundResource(R.drawable.play_button);
        record=(Button)findViewById(R.id.record);
        record.setBackgroundResource(R.drawable.record_button);
        play.setOnClickListener(new View.OnClickListener()
	    {
			 public void onClick(View v)
	         {
				 System.out.println("Play button clicked");
				 
				 onPlay(mStartPlaying);
	                if (mStartPlaying) 
	                {
	                	play.setBackgroundResource(R.drawable.stop_button);   
	                } 
	                else 
	                {
	                	play.setBackgroundResource(R.drawable.play_button);
	                }
	                mStartPlaying = !mStartPlaying;
				 
	         }
	    });
        record.setOnClickListener(new View.OnClickListener()
	    {
			 public void onClick(View v)
	         {
				 howToInsert=1;
				 onRecord(mStartRecording);
	                if (mStartRecording) 
	                {
	                	record.setBackgroundResource(R.drawable.record_stop);
	                } 
	                else 
	                {
	                	record.setBackgroundResource(R.drawable.record_button);
	                }
	                mStartRecording = !mStartRecording;
				 
	         }
	    });
        display(); 
      } 

	private void display() 
	{
		System.out.println("data setting");
		String selectQuery = "SELECT  * FROM " + DatabaseNotes.NOTES_TABLE_NAME +" where "+DatabaseNotes.NOTES_ID+" = '"+i.getStringExtra("notesid")+"'";
    	Cursor cursor = mDB.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) 
        {
	          do 
	          {
	        	  title.setText(cursor.getString(cursor.getColumnIndex("notes_title")));
	        	  String str=cursor.getString(cursor.getColumnIndex("notes_data"));
	        	  System.out.println(str);
	        	  data.setText(str);
	        	  latitude=cursor.getDouble(cursor.getColumnIndex("notes_latitude"));
	        	  longitude=cursor.getDouble(cursor.getColumnIndex("notes_longitude")); 
	          } while (cursor.moveToNext());
        }
        System.out.println("audio");
        selectQuery="Select * from "+ DatabaseNotesFiles.NOTES_FILES_TABLE_NAME+" where "+DatabaseNotesFiles.NOTES_ID+" = '"+i.getStringExtra("notesid")+"'";
        Cursor cursor1=mDB.rawQuery(selectQuery, null);
        cursor1.moveToFirst();
        mFileName=cursor1.getString(cursor1.getColumnIndex("notes_audio_path"));
        if(!(mFileName.equals("")))
        {
        	play.setVisibility(View.VISIBLE);
        	record.setVisibility(View.GONE);
        }
        else
        {
        	play.setVisibility(View.GONE);
        	record.setVisibility(View.GONE);
        }
        System.out.println("Image setting");
        System.out.println("id is "+i.getStringExtra("notesid"));
        selectQuery="Select * from "+ DatabaseNotesPictures.NOTES_PICTURES_TABLE_NAME+" where "+DatabaseNotes.NOTES_ID+" = '"+i.getStringExtra("notesid")+"'";
        Cursor cursor2=mDB.rawQuery(selectQuery, null);
        //cursor2.moveToFirst();
        if( cursor2.moveToFirst())
        {
        	String noteid=cursor2.getString(cursor2.getColumnIndex("_id"));
        	System.out.println("note id fetched is"+noteid);
	        mImagePath=cursor2.getBlob(cursor2.getColumnIndex("notes_picture_path"));
	        Bitmap bitmap = BitmapFactory.decodeByteArray(mImagePath , 0, mImagePath .length);
	        image.setImageBitmap(bitmap);
        }
       
        
	}
	public  void update(SQLiteDatabase db,String notes_title,String notes_data,String subject_name) 
    {
		ContentValues values = new ContentValues();
		System.out.println(i.getStringExtra("notesid"));
		values.put("notes_title",notes_title);
		values.put("notes_data", notes_data);
		values.put("subject_name", subject_name);
		mDB.update("notes", values, DatabaseNotes.NOTES_ID+" = ?",new String[] {i.getStringExtra("notesid")});
		Log.i(DEBUG_TAG, "UPDATED Notes:  " + notes_title+ " "+notes_data+"(ID=" + i+ ")");
		System.out.println("updated");
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		if(howToInsert==0)
			mFileName=path;
			ContentValues value1=new ContentValues();
			value1.put("notes_audio_path", mFileName);
			mDB.update("notes_audio_files", value1, DatabaseNotes.NOTES_ID+" = ?",new String[] {i.getStringExtra("notesid")});
			System.out.println("Audio Updated");
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("image insertion");
		ContentValues value2=new ContentValues();
		System.out.println("before inserting image"+mImagePath);
		value2.put("notes_picture_path", mImagePath);
		mDB.update("notes_picture_files", value2, DatabaseNotes.NOTES_ID+" = ?",new String[] {i.getStringExtra("notesid")});
		System.out.println("Image Updated");
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
	    // Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.editnotes1, menu);
	    return true;
	}
	
	private void onRecord(boolean start) 
    {
        if (start) 
        {
            startRecording();
        }
        else 
        {
            stopRecording();
        }
    }

    private void onPlay(boolean start) 
    {
        if (start) 
        {
            startPlaying();
        } 
        else 
        {
            stopPlaying();
        }
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
        		mPlayer.setDataSource(mFileName);
	            mPlayer.prepare();
	            mPlayer.start();
        	}
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }

    private void stopPlaying() 
    {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() 
    {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        AudioRecordTest();
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try 
        {
            mRecorder.prepare();
        } 
        catch (IOException e)
        {
            e.printStackTrace();
        }

        mRecorder.start();
    }

    private void stopRecording() 
    {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        play.setVisibility(View.VISIBLE);
        record.setVisibility(View.GONE);
    }
   
    public void AudioRecordTest() 
    {
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFileName += "/audiorecordtest.3gp";
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    int id = item.getItemId();
	    if (id == R.id.save_notes) 
	    {
	    	update(mDB,String.valueOf(title.getText()),String.valueOf(data.getText()),subject_name);
	    	Intent i=new Intent(getApplicationContext(),NotesActivity.class);
	    	i.putExtra("subject_name", subject_name);
	    	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(i);
	    	
	    }
	    else if(id==R.id.seemap)
	    {
	    	showMap();
	    }
	    else if(id==R.id.takeaudio)
	    {
	    	audio();
	    }
	    else if(id==R.id.takepicture)
	    {
	    	AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditNotesActivity.this);
	        // Setting Dialog Title
	        alertDialog.setTitle("Select:");
	        alertDialog.setPositiveButton("CAMERA", new DialogInterface.OnClickListener() 
	        {
	                    public void onClick(DialogInterface dialog,int which) 
	                    {
	                    	Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	                        if (takePictureIntent.resolveActivity(getPackageManager()) != null)
	                        {
	                            startActivityForResult(takePictureIntent, 1);
	                        }
	                    }
	         });
	         //Setting Negative "NO" Button
	       alertDialog.setNegativeButton("GALLERY",new DialogInterface.OnClickListener() 
	        {
	                    public void onClick(DialogInterface dialog, int which) 
	                    {
	                        // Write your code here to execute after dialog
	                    	Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	                    	startActivityForResult(intent, 0);
	                        dialog.cancel();
	                    }
	                });
	        alertDialog.show();  
	       
	    }
	
	    return super.onOptionsItemSelected(item);
	}
	
	private void audio() 
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditNotesActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("Select:");
        alertDialog.setPositiveButton("RECORD", new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog,int which) 
                    {
                    	play.setVisibility(View.GONE);
                    	record.setVisibility(View.VISIBLE);
                    	flag=0;
                    	
                    }
         });
         //Setting Negative "NO" Button
       alertDialog.setNegativeButton("MEDIA PLAYER",new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog, int which) 
                    {
                        // Write your code here to execute after dialog
                    	flag=1;
                    	howToInsert=0;
                    	Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                    	startActivityForResult(galleryIntent,2);
                    	
                    }
                });
        alertDialog.show(); 
		
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data1) 
    {
		Bitmap imageBitmap = null;
    	super.onActivityResult(requestCode, resultCode, data1);
        if (requestCode == 1 && resultCode == RESULT_OK) 
        {
        	System.out.println("camera click");
            Bundle extras = data1.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            image.setImageBitmap(imageBitmap);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
		       imageBitmap.compress(CompressFormat.JPEG, 100, baos);
		       mImagePath = baos.toByteArray();
        }
        else if(requestCode == 0 )
        {
    	    if (resultCode == RESULT_OK)
    	    {
    		     Uri targetUri = data1.getData();
    		     try 
    		     {
	    		      imageBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
	    		      System.out.println("IMAGE PATH IS "+targetUri);
	    		      Matrix matrix = new Matrix();
	    		      matrix.postRotate(270);
	    		      imageBitmap=Bitmap.createScaledBitmap(imageBitmap, 200,200, true);
	    		      imageBitmap = Bitmap.createBitmap(imageBitmap , 0, 0, imageBitmap .getWidth(), imageBitmap .getHeight(), matrix, true);
	    		      image.setImageBitmap(imageBitmap);
	    		       //imagePath=String.valueOf(targetUri);
	    		       ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    		       imageBitmap.compress(CompressFormat.JPEG, 100, baos);
	    		       mImagePath = baos.toByteArray();
	    		      /*ss = new SpannableString(data.getText());
	    		      //Drawable d = mImageView.getDrawable();
	    			  @SuppressWarnings("deprecation")
					  ImageSpan span = new ImageSpan(imageBitmap);
	    			  ss.setSpan(span,data.getText().length()-1,data.getText().length(), 0);
	    		     
	    		      toSend=Html.toHtml(ss); 
	    		      data.setText(toSend);*/
    		     } 
    		     catch (FileNotFoundException e) 
    		     {
    		    	  e.printStackTrace();
    		     }
    	    }
        }
        
    }

	private void showMap() 
	{
		final String geoURI = String.format(Locale.getDefault(), "geo: %f,%f",latitude, longitude);
		Intent map = new Intent(Intent.ACTION_VIEW, Uri.parse(geoURI));
		startActivity(map);
		
	}
}
