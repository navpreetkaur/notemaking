package com.example.notemakingapplication;

import android.provider.BaseColumns;

public class DatabaseEntities 
{
private DatabaseEntities(){}
	
	public static final class DatabaseSubject implements BaseColumns
	{
		private DatabaseSubject() {}
		public static final String SUBJECT_TABLE_NAME = "subjects";
		public static final String SUBJECT_ID = "_id";
		public static final String SUBJECT_NAME = "subject_name";
		public static  String DEFAULT_SORT_ORDER = "_id ASC";
	}
	
	public static final class DatabaseNotes implements BaseColumns
	{
		private DatabaseNotes() {}
		public static final String NOTES_TABLE_NAME = "notes";
		public static final String NOTES_ID = "_id";
		public static final String NOTES_TITLE = "notes_title";
		public static final String NOTES_DATA = "notes_data";
		public static final String NOTES_DATE = "notes_date";
		public static final String NOTES_TIME = "notes_time";
		public static final String NOTES_LATITUDE="notes_latitude";
		public static final String NOTES_LONGITUDE="notes_longitude";
		public static String DEFAULT_SORT_ORDER = "_id ASC";
	}
	
	public static final class DatabaseNotesFiles implements BaseColumns
	{
		private DatabaseNotesFiles() {}
		public static final String NOTES_FILES_TABLE_NAME = "notes_audio_files";
		public static final String NOTES_ID = "_id";
		public static final String NOTES_AUDIO_PATH="notes_audio_path";
		public static String DEFAULT_SORT_ORDER = "_id ASC";
	}
	
	public static final class DatabaseNotesPictures implements BaseColumns
	{
		private DatabaseNotesPictures() {}
		public static final String NOTES_PICTURES_TABLE_NAME = "notes_picture_files";
		public static final String NOTES_ID = "_id";
		public static final String NOTES_PICTURE_PATH="notes_picture_path";
		public static String DEFAULT_SORT_ORDER = "_id ASC";
	}
	

}
