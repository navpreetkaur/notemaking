package com.example.notemakingapplication;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class AddNotesActivity extends Activity implements LocationListener {
	private android.hardware.Camera mCameraDevice;
	LocationManager location = null;
	LocationListener loc = null;
	private static final String DEBUG_TAG = "SimpleDB Log";
	protected DatabaseHelper mDatabase = null;
	protected Cursor mCursor = null;
	protected SQLiteDatabase mDB = null;
	EditText title, data;
	ImageView mImageView;
	Intent i;
	String subject_name;
	Button play, record;
	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_IMAGE_FETCH = 0;
	static final int REQUEST_AUDIO_FETCH = -1;
	boolean mStartPlaying = true;
	boolean mStartRecording = true;
	int flag = 0;
	int howToInsert = 0;
	double latitude = 0, longitude = 0;
	SpannableString ss;
	String toSend;
	String filename;
	String path = "";
	String imagePath;
	byte[] imageData = new byte[]{};


	private static final String LOG_TAG = "AudioRecordTest";
	private static String mFileName = null;
	private MediaRecorder mRecorder = null;

	private MediaPlayer mPlayer = null;


	private void onRecord(boolean start) {
		if (start) {
			startRecording();
		} else {
			stopRecording();
		}
	}

	private void onPlay(boolean start) {
		if (start) {
			startPlaying();
		} else {
			stopPlaying();
		}
	}

	private void startPlaying() {
		mPlayer = new MediaPlayer();
		try {
			if (path.equals("") && flag == 1) {

			} else {
				if (flag == 1)
					mPlayer.setDataSource(path);
				else
					mPlayer.setDataSource(mFileName);
				mPlayer.prepare();
				mPlayer.start();
			}
		} catch (IOException e) {
			Log.e(LOG_TAG, "prepare() failed");
		}
	}

	private void stopPlaying() {
		mPlayer.release();
		mPlayer = null;
	}

	private void startRecording() {
		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		AudioRecordTest();
		mRecorder.setOutputFile(mFileName);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

		try {
			mRecorder.prepare();
		} catch (IOException e) {
			Log.e(LOG_TAG, "prepare() failed");
		}

		mRecorder.start();
	}

	private void stopRecording() {
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
		play.setVisibility(View.VISIBLE);
		record.setVisibility(View.GONE);
	}

	public void AudioRecordTest() {
		mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
		mFileName += "/audiorecordtest.3gp";

	}

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		findLocation();
		setContentView(R.layout.activity_add_notes);
		title = (EditText) findViewById(R.id.notestitle);
		data = (EditText) findViewById(R.id.notesdata);
		play = (Button) findViewById(R.id.play);
		play.setBackgroundResource(R.drawable.play_button);
		record = (Button) findViewById(R.id.record);
		record.setBackgroundResource(R.drawable.record_button);
		if (path.equals("")) {
			play.setVisibility(View.GONE);

		}
		record.setVisibility(View.GONE);
		play.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				System.out.println("button clicked");

				onPlay(mStartPlaying);
				if (mStartPlaying) {
					play.setBackgroundResource(R.drawable.stop_button);

				} else {
					play.setBackgroundResource(R.drawable.play_button);
				}
				mStartPlaying = !mStartPlaying;

			}
		});
		record.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				howToInsert = 1;
				onRecord(mStartRecording);
				if (mStartRecording) {
					record.setBackgroundResource(R.drawable.record_stop);
				} else {
					record.setBackgroundResource(R.drawable.record_button);
				}
				mStartRecording = !mStartRecording;

			}
		});
		mDatabase = new DatabaseHelper(this.getApplicationContext());
		mDB = mDatabase.getWritableDatabase();
		mImageView = (ImageView) findViewById(R.id.mImageView);
		i = getIntent();
		subject_name = i.getStringExtra("subject_name");
	}

	public void insert(SQLiteDatabase db, String notes_title, String notes_data, String subject_name) throws IOException {
		ContentValues values = new ContentValues();
		values.put("notes_title", notes_title);
		values.put("notes_data", notes_data);
		values.put("subject_name", subject_name);
		values.put("notes_latitude", latitude);
		values.put("notes_longitude", longitude);
		long i = db.insertOrThrow("notes", null, values);
		Log.i(DEBUG_TAG, "Added Notes:  " + notes_title + " " + filename + "(ID=" + i + ")");
		///////////////////////////////////////////////////////////////////////////////////////
		if (howToInsert == 0)
			mFileName = path;
		ContentValues value1 = new ContentValues();
		value1.put("_id", i);
		System.out.println("before inserting " + mFileName);
		value1.put("notes_audio_path", mFileName);
		long j = db.insertOrThrow("notes_audio_files", null, value1);
		Log.i(DEBUG_TAG, "Added Notes FILES:  " + mFileName + "(ID=" + j + ")");
		///////////////////////////////////////////////////////////////////////////////////////
		System.out.println("image insertion");
		ContentValues value2 = new ContentValues();
		value2.put("_id", i);
		System.out.println("before inserting image" + imageData);
		value2.put("notes_picture_path", imageData);
		long k = db.insertOrThrow("notes_picture_files", null, value2);
		Log.i(DEBUG_TAG, "Added Notes FILES:  " + imageData + "(ID=" + k + ")");
	}

	@TargetApi(Build.VERSION_CODES.M)
	private void findLocation() {
		System.out.println("find location");
		location = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		System.out.println("manager created");
		Criteria criteria = new Criteria();
		System.out.println("criteria created");
		criteria.setAccuracy(Criteria.NO_REQUIREMENT);
		System.out.println("setaccuracy");
		criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
		System.out.println("set power");
		String best = location.getBestProvider(criteria, true);
		System.out.println("best");

		if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    Activity#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for Activity#requestPermissions for more details.
			return;
		}
		location.requestLocationUpdates(best, 1000000000, 0, AddNotesActivity.this);
		
	}
	public void onLocationChanged(Location location) 
	{
		System.out.println(location.getLatitude());
		System.out.println(location.getLongitude());
		latitude=location.getLatitude();
		longitude=location.getLongitude();
		final String geoURI = String.format(Locale.getDefault(), "geo: %f,%f",location.getLatitude(), location.getLongitude());
		System.out.println("geo uri is"+geoURI);
	}
		
	private static final SparseArray<String> providerStatusMap = new SparseArray<String>() {
		{
			put(LocationProvider.AVAILABLE, "Available");
			put(LocationProvider.OUT_OF_SERVICE, "Out of Service");
			put(LocationProvider.TEMPORARILY_UNAVAILABLE,
					"Temporarily Unavailable");
			put(-1, "Not Reported");
		}
	};

	public void onStatusChanged(String provider, int status, Bundle extras) {
		int satellites = extras.getInt("satellites", -1);

		String statusInfo = String.format(Locale.getDefault(),
				"Provider: %s, status: %s, satellites: %d", provider,
				providerStatusMap.get(status), satellites);
		Log.v("GPS", statusInfo);
		

	}
	    
	    
	    public void onProviderDisabled(String provider) {
			Log.v("GPS", "Provider disabled " + provider);

		}

		public void onProviderEnabled(String provider) {
			Log.v("GPS", "Provider enabled " + provider);

		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
	    // Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.editnotes, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    int id = item.getItemId();
	    if (id == R.id.save_notes) 
	    {
	    	try
	    	{
	    		insert(mDB,String.valueOf(title.getText()),data.getText().toString(),subject_name);
	    	}
	    	catch(IOException e)
	    	{
	    		e.printStackTrace();
	    	}
	    	Intent i=new Intent(getApplicationContext(),NotesActivity.class);
	    	i.putExtra("subject_name", subject_name);
	    	i.putExtra("any","any");
	    	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        	startActivity(i);
	    }
	    else if (id == R.id.takepicture) 
	    {
	    	pictures();
	    }
	    else if (id == R.id.takeaudio) 
	    {
	    	audio();
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	private void audio() 
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddNotesActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("Select:");
        alertDialog.setPositiveButton("RECORD", new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog,int which) 
                    {
                    	play.setVisibility(View.GONE);
                    	record.setVisibility(View.VISIBLE);
                    	flag=0;
                    	
                    }
         });
         //Setting Negative "NO" Button
       alertDialog.setNegativeButton("MEDIA PLAYER",new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog, int which) 
                    {
                        // Write your code here to execute after dialog
                    	flag=1;
                    	howToInsert=0;
                    	Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                    	startActivityForResult(galleryIntent,2);
                    	
                    }
                });
        alertDialog.show(); 
		
	}
	private void pictures() 
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddNotesActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("Select:");
        alertDialog.setPositiveButton("CAMERA", new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog,int which) 
                    {
                    	try 
                    	{
                    	  //mCameraDevice = android.hardware.Camera.open();
                    	  Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                          if (takePictureIntent.resolveActivity(getPackageManager()) != null)
                          {
                              startActivityForResult(takePictureIntent, 1);
                          }
                    	} 
                    	catch (RuntimeException e) 
                    	{
                    		AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(AddNotesActivity.this);
                    		alertDialog1.setTitle("Camera");
                            alertDialog1.setPositiveButton("OK!!", new DialogInterface.OnClickListener() 
                            {
                                        public void onClick(DialogInterface dialog,int which) 
                                        {
                                        	dialog.cancel();
                                        }
                             });
                    	}
                    	
                    }
         });
         //Setting Negative "NO" Button
       alertDialog.setNegativeButton("GALLERY",new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog, int which) 
                    {
                        // Write your code here to execute after dialog
                    	Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    	startActivityForResult(intent, 0);
                        dialog.cancel();
                    }
                });
        alertDialog.show();  
       
	}
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data1) 
    {
		Bitmap imageBitmap = null;
    	super.onActivityResult(requestCode, resultCode, data1);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) 
        {
        	System.out.println("camera click");
            Bundle extras = data1.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            mImageView.setImageBitmap(imageBitmap);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
		       imageBitmap.compress(CompressFormat.JPEG, 100, baos);
		      imageData = baos.toByteArray();
        }
        else if(requestCode == 0 )
        {
    	    if (resultCode == RESULT_OK)
    	    {
    		     Uri targetUri = data1.getData();
    		     try 
    		     {
	    		      imageBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
	    		      System.out.println("IMAGE PATH IS "+targetUri);
	    		      Matrix matrix = new Matrix();
	    		      matrix.postRotate(0);
	    		      imageBitmap=Bitmap.createScaledBitmap(imageBitmap, 200,200, true);
	    		      imageBitmap = Bitmap.createBitmap(imageBitmap , 0, 0, imageBitmap .getWidth(), imageBitmap .getHeight(), matrix, true);
	    		      mImageView.setImageBitmap(imageBitmap);
	    		       //imagePath=String.valueOf(targetUri);
	    		       ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    		       imageBitmap.compress(CompressFormat.JPEG, 100, baos);
	    		      imageData = baos.toByteArray();
	    		      /*ss = new SpannableString(data.getText());
	    		      //Drawable d = mImageView.getDrawable();
	    			  @SuppressWarnings("deprecation")
					  ImageSpan span = new ImageSpan(imageBitmap);
	    			  ss.setSpan(span,data.getText().length()-1,data.getText().length(), 0);
	    		     
	    		      toSend=Html.toHtml(ss); 
	    		      data.setText(toSend);*/
    		     } 
    		     catch (FileNotFoundException e) 
    		     {
    		    	  e.printStackTrace();
    		     }
    	    }
        }
        else if(requestCode==2)
        {
                if (resultCode == Activity.RESULT_OK) 
                {
                    try 
                    {
	    				path = _getRealPathFromURI(getApplicationContext(), data1.getData());
	    				play.setVisibility(View.VISIBLE);
                    } 
                    catch (Exception e) 
                    {
                        e.printStackTrace();
                    }
                }
        }
    }
	private String _getRealPathFromURI(Context context, Uri contentUri) 
    {
        String[] proj = { MediaStore.Audio.Media.DATA };
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
