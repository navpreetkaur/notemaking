package com.example.notemakingapplication;

import com.example.notemakingapplication.DatabaseEntities.DatabaseNotes;
import com.example.notemakingapplication.DatabaseEntities.DatabaseSubject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class SubjectActivity extends Activity 
{
	private static final String DEBUG_TAG = "SimpleDB Log";
	protected  DatabaseHelper mDatabase = null; 
	protected Cursor mCursor = null;
	protected SQLiteDatabase mDB = null;
	private SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	private String asColumnsToReturn[] = {
			DatabaseSubject.SUBJECT_ID,
			DatabaseSubject.SUBJECT_NAME };
	private ListAdapter adapter = null;
	private ListView av = null;
	ListView listView;
	
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject); 
        mDatabase = new DatabaseHelper(this.getApplicationContext());
		mDB = mDatabase.getWritableDatabase();	
		displaySubject();
		listView=(ListView)findViewById(R.id.subjectList);
    }
	public  void insert(SQLiteDatabase db,String subjectname) 
    {
    	ContentValues values = new ContentValues();
			values.put("subject_name", subjectname);
			long i = db.insertOrThrow("subjects", null,values);
			Log.i(DEBUG_TAG, "Added Subject:  " + subjectname+ "(ID=" + i+ ")");
			displaySubject();
		
	}
	private void displaySubject()
	{
		queryBuilder.setTables(DatabaseSubject.SUBJECT_TABLE_NAME );
    	mCursor = queryBuilder.query(mDB, asColumnsToReturn, null, null, null,
				null,DatabaseSubject.DEFAULT_SORT_ORDER);
    	adapter = new SimpleCursorAdapter(this, R.layout.activity_show_subjects,
				mCursor, new String[] {  DatabaseSubject.SUBJECT_NAME},
				new int[] { R.id.TextView_subjectname}, 1);		
		av = (ListView) findViewById(R.id.subjectList);
		av.setAdapter(adapter);
		av.setOnItemClickListener(new OnItemClickListener() 
		{
            public void onItemClick(AdapterView <? > parent, View view,
    	            int position, long id) 
            {
            	TextView txtview= (TextView) view.findViewById(R.id.TextView_subjectname);
            	String subject_name = txtview.getText().toString();
            	System.out.println("TEXTVIEW IS "+subject_name);
            	Intent i=new Intent(getApplicationContext(),NotesActivity.class);
            	i.putExtra("subject_name", subject_name);
            	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            	i.putExtra("subjectname", subject_name);
            	startActivity(i);
            }
			

        });	
		
		av.setOnItemLongClickListener(new OnItemLongClickListener() 
		{
			
	        @Override
	        public boolean onItemLongClick(AdapterView<?> parent, final View view, int position, long id) 
	        {
	        	
	        	AlertDialog.Builder alertDialog = new AlertDialog.Builder(SubjectActivity.this);
	            // Setting Dialog Title
	            alertDialog.setTitle("Delete Subject");
	            // Setting Dialog Message
	            alertDialog.setMessage("Do you really want to delete this subject?");
	            alertDialog.setPositiveButton("Delete",
	                    new DialogInterface.OnClickListener() 
	            {
	                        public void onClick(DialogInterface dialog,int which) 
	                        {
	                        	TextView txtview= (TextView) view.findViewById(R.id.TextView_subjectname);
	                        	String subject_name = txtview.getText().toString();
	                        	System.out.println("Deleted"+subject_name);
	                        	mDB.delete(DatabaseNotes.NOTES_TABLE_NAME,"subject_name=?",new String[] { subject_name });
	                        	mDB.delete(DatabaseSubject.SUBJECT_TABLE_NAME,"subject_name=?",new String[] { subject_name });
	                        	displaySubject();
	                            
	                        }
	             });
	            // Setting Negative "NO" Button
	            alertDialog.setNegativeButton("Cancel",
	                    new DialogInterface.OnClickListener() 
	            {
	                        public void onClick(DialogInterface dialog, int which) 
	                        {
	                            dialog.cancel();
	                        }
	                    });

	            // closed

	            // Showing Alert Message
	            alertDialog.show(); 
	            return true;
	            
	        }
	        
	    });

	}
	public void onItemClick(AdapterView<?> parent, View view,
			int position, long id) 
	{
		
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
	    // Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.subject, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    int id = item.getItemId();
	    if (id == R.id.add_subject) 
	    {
	        show_alert();
	    }
	    return super.onOptionsItemSelected(item);
	}
	public void show_alert()
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(SubjectActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("Subject");
        // Setting Dialog Message
        alertDialog.setMessage("Enter Subject");
        final EditText input = new EditText(SubjectActivity.this);  
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                              LinearLayout.LayoutParams.MATCH_PARENT,
                              LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input); 

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.key);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("SAVE",
                new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog,int which) 
                    {
                        // Write your code here to execute after dialog
                        //Toast.makeText(getApplicationContext(),"Saved", Toast.LENGTH_SHORT).show();
                    	String subjectname=String.valueOf(input.getText());
                    	insert(mDB,subjectname);
                    	Toast.makeText(getApplicationContext(),"Subject Added", Toast.LENGTH_SHORT).show();
                        
                    }
         });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() 
        {
                    public void onClick(DialogInterface dialog, int which) 
                    {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show(); 
	}
}



